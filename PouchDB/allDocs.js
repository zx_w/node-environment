const utils = require('./utils');

utils.db.allDocs({
    include_docs: true,
    keys: []
})
    .then(r => console.log('result', r.rows))
    .catch(e => console.log('error', e));