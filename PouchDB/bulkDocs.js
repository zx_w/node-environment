const utils = require('./utils');

utils.db.bulkDocs([
    {
        _id: utils.ids[0],
        content: 'AAA'
    }, {
        _id: utils.ids[1],
        content: 'BBB'
    }, {
        _id: utils.ids[2],
        content: 'CCC'
    }]
)
    .then(r => console.log('result', r))
    .catch(e => console.log('error', e));