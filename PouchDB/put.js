const utils = require('./utils');

utils.db.put({
    _id: utils.id,
    content: '笑嘻嘻',
    _attachments: {
        map: {
            content_type: 'text/plain',
            data: new Buffer(['Hello World!'], {type: 'text/plain'})
        },
        sensor: {
            content_type: 'text/plain',
            data: new Buffer(['Halo！'], {type: 'text/plain'})
        }
    }
})
    .catch(e => console.log('error', e));