const utils = require('./utils');

utils.db.createIndex({
    index: {
        fields: ['timestamp']
    }
})
    .catch(e => console.log(e, e.message, e.reason));