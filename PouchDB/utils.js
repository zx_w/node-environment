let PouchDB = require('pouchdb');
let PouchDBUpsert = require('pouchdb-upsert');
let PouchDBFind = require('pouchdb-find');

PouchDB.plugin(PouchDBUpsert);
PouchDB.plugin(PouchDBFind);

const ids = [
    '333333',
    '111111',
    '222222'
];
const id = ids[0];
const pouchName = 'Bucket';
const pouchUrl = 'http://localhost:4984/db';
const db = new PouchDB(pouchUrl);

module.exports = {ids, id, db};

db.changes({
    since: 'now',
    live: true,
    include_docs: true,
    attachments: true
}).on('change', c => {
    console.log('change', c.doc._attachments);
});