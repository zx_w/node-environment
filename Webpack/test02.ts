import defaultsDeep from 'lodash/defaultsDeep.js';

let args = {'a': {'b': 2}};
let options = {'a': {'b': 1, 'c': 3}};
defaultsDeep(args, options);