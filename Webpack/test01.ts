import * as _ from 'lodash';

let args = {'a': {'b': 2}};
let options = {'a': {'b': 1, 'c': 3}};
console.log(_.defaultsDeep(args, options));