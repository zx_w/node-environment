function example() {
    let returnState = false; // initialisation value is really up to the design
    console.log('init', returnState);
    try {
        returnState = true;
        console.log('try', returnState);
    } catch (e) {
        returnState = false;
        console.log('catch', returnState);
    } finally {
        console.log('finally', returnState);
        return returnState;
    }
}

let a = example();
console.log(a);