const path = require('path');

module.exports = {
    entry: {
        // 'test01': __dirname + '/test01.ts',
        'test02': __dirname + '/test02.ts',
        // 'test03': __dirname + '/test03.ts',
        // 'test04': __dirname + '/test04.ts'
    }, // 入口
    output: { // 输出
        path: __dirname + '/dist',
        filename: '[name].js'
    },
    module: {
        rules: [
            {test: /\.ts$/, loader: 'ts-loader'}
        ]
    },
    devtool: false
};