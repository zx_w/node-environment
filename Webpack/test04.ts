import * as Rx from 'rxjs/BehaviorSubject';

let subject = new Rx.BehaviorSubject<number>(0);
subject.subscribe(n => console.log(n));