/**
 * public buffer(closingNotifier: Observable<any>): Observable<T[]>
 *
 * public bufferCount(bufferSize: number, startBufferEvery: number): Observable<T[]>
 *
 * public bufferTime(bufferTimeSpan: number, bufferCreationInterval: number, maxBufferSize: number, scheduler: Scheduler): Observable<T[]>
 *
 * public bufferToggle(openings: SubscribableOrPromise<O>, closingSelector: (value: O) => SubscribableOrPromise): Observable<T[]>
 *
 * public bufferWhen(closingSelector: () => Observable): Observable<T[]>
 */

let Rx = require('rxjs');

let count = 1;
let duration = 1000;
let subject = new Rx.Subject();
setInterval(() => {
    subject.next(count++);
    console.time('t');
}, 200);

subject
    .buffer(Rx.Observable.timer(duration))
    .subscribe(array => console.log('buffer', array));
subject
    .bufferCount(2, 1)
    .subscribe(array => console.log('bufferCount', array));
subject
    .bufferTime(duration, duration)
    .subscribe(array => console.log('bufferTime', array));
subject
    .bufferToggle(Rx.Observable.interval(duration), count => count % 2 ? Rx.Observable.timer(duration * 0.8) : Rx.Observable.empty())
    .subscribe(array => console.log('bufferToggle', array));
subject
    .bufferWhen(() => Rx.Observable.interval(duration + Math.random() * 2 * duration))
    .subscribe(array => console.log('bufferWhen', array));