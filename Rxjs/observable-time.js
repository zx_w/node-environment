/**
 * 测试
 * throttleTime
 * debounceTime
 * auditTime
 */
let Rx = require('rxjs');

let count = 1;
let duration = 1000;
let subject = new Rx.Subject();
setInterval(() => {
    subject.next(count++);
    console.time('t');
}, 200);

subject
    .throttleTime(duration)
    .subscribe(value => console.log('throttleTime', value));
subject
    .throttle(() => Rx.Observable.timer(duration))
    .subscribe(value => console.log('throttle', value));

subject
    .debounceTime(duration)
    .subscribe(value => console.log('debounceTime', value));
subject
    .debounce(() => Rx.Observable.timer(duration))
    .subscribe(value => console.log('debounce', value));

subject
    .auditTime(duration)
    .subscribe(value => console.log('auditTime', value));
subject
    .audit(() => Rx.Observable.timer(duration))
    .subscribe(value => console.log('audit', value));