/**
 * public catch(selector: function): Observable
 */
let Rx = require('rxjs');

let count = 1;
let subject = new Rx.Subject();
setInterval(() => {
    if (count === 16)
        subject.error('发生错误');
    else
        subject.next(count++);
}, 200);

subject
    .catch(error => console.log('catch', error))
    .subscribe(
        value => console.log('value', value),
        error => console.log('error', error),
        complete => console.log('complete', complete)
    );

Rx.Observable.of(1, 2, 3, 4, 5)
    .map(num => {
        if (num === 4) {
            throw '发生错误';
        }
        return num;
    })
    .catch(error => Rx.Observable.of('I', 'II', 'III', 'IV', 'V'))
    .subscribe(value => console.log('value', value));