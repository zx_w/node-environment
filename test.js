let Mustache = require('mustache');
// let fs = require('fs');
let option = {
    cluster: 'PROD-A',
    mode: 'dev',
    username: 'r3t',
    key: 'path to ssh private key',
    servers: [{
        host: 'a-01',
        ip: '192.168.101.231'
    }, {
        host: 'a-02',
        ip: '192.168.101.233'
    }],
    db: {
        hosts: ['a-02', 'a-01'],
        ramSize: 1536,
        indexSize: 512
    },
    sg: 'a-01',
    service: 'a-02',
    storage: {
        entry: '0ee25482e9-swx16.cn-hangzhou.nas.aliyuncs.com',
        path: '/mnt/storage'
    }
};
// let dbs = option.dbs;
// let hosts = '127.0.0.1 localhost';
// hosts += `\n\n# 机器列表`;
// for (let key in option.servers)
//     hosts += `\n${option.servers[key]} ${key}`; // 机器列表
// hosts += `\n\n# 数据库入口\n${option.servers[dbs[0]]} cb`; // 数据库入口
// hosts += `\n\n# 数据库列表`;
// for (let db of dbs)
//     hosts += `\n${option.servers[db]} ${db}`; // 数据库列表
// hosts += `\n\n# sync_gateway入口\n${option.servers[option.sg]} sg`; // sync_gateway入口
// hosts += `\n\n# 程序入口\n${option.servers[option.service]} sv`; // 程序入口
// fs.appendFileSync('./Rxjs/hosts', hosts);

// console.log(option.servers.findIndex(item => item.host === 'a-02'));

let hosts = `127.0.0.1 localhost\n\n`;
hosts += `# 机器列表\n${Mustache.render('{{#servers}}{{ip}} {{host}}\n{{/servers}}', option)}\n\n`;
hosts += `# 数据库入口\n${option.servers[1].ip} cb\n\n`;
hosts += `# sync_gateway入口\n${option.servers[option.servers.findIndex(item => item.host === option.sg)].ip} sg\n\n`;
hosts += `# 程序\n${option.servers[option.servers.findIndex(item => item.host === option.service)].ip} sv`;
console.log(hosts);

let fstab = `${Mustache.render('{{{storage.entry}}} {{{storage.path}}}', option)} nfs4 defaults 0 0`;
console.log(fstab);